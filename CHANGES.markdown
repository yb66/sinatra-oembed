# Ch-ch-ch-ch-changes #

## Thursday the 29th of November 2012, v0.0.5 ##

Fix for the Faraday/Typhoeus requiring. It turns out that sometimes Faraday/Typhoeus needs `require 'typhoeus/adapters/faraday'` to work, and others `require 'faraday/adapter/typhoeus'` so you take your pick. If it's the later, use v0.0.4. If it's the former, then use this version, 0.0.5.

## Thursday the 29th of November 2012, v0.0.4 ##

* The change in 0.0.3 was wrong, fixed.
* The Faraday library introduced an undocumented error - "Ethon::Errors::InvalidOption - The option: disable_ssl_peer_verification is invalid." Fixed.

## Wednesday the 7th of November 2012, v0.0.3 ##

* All errors raised are tagged with the namespace, so that consumers can work out where it came from and handle better.

---

## Monday the 24th of September 2012, v0.0.2 ##

* Can build documentation via yard now.

---

## Friday the 3rd of August 2012, v0.0.1 ##

* First gem.

----