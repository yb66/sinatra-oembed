# encoding: UTF-8

require 'sinatra/base'
require 'faraday'
require 'faraday_middleware'
#require 'faraday/adapter/typhoeus' # see https://github.com/typhoeus/typhoeus/issues/226#issuecomment-9919517
require 'typhoeus/adapters/faraday'
require 'json'

module Sinatra
  module OEmbed

    module Error; end

    # namespaces any errors raised, for the convenience of consumers
    def self.tag_errors
      yield
    rescue Exception => error
      error.extend(OEmbed::Error)
      raise
    end
      
    module Helpers
    
      # @param [String] url URL to get details for.
      # @return [String] JSON will come back, either with the oembed data as a hash.
      def get_json_from_endpoint_for(url, opts={provider_url: "http://www.youtube.com/oembed?url=", format: "json"} )
        warn "Entering get_json_from_endpoint_for"
        OEmbed.tag_errors do
          response = Faraday.get "#{opts[:provider_url]}#{url}&format=#{opts[:format]}"
          response.body
        end
      end

    end


    # @param [String] user_agent The string to use as the user agent when making requests. Should probably be the url of your site. Defaults to 'Faraday Ruby Client'.
    def oembed_user_agent=( user_agent )
      @oembed_user_agent = user_agent
    end


    # @return [String] The user agent used when making requests.    
    def oembed_user_agent
      @oembed_user_agent ||= 'Faraday Ruby Client'
    end


    # @param [Sinatra::Base] app
    # @private
    def self.registered(app)
      app.configure do
        
        Faraday.default_connection = Faraday::Connection.new( 
          :headers => { :accept =>  'application/json',
          :user_agent => app.settings.oembed_user_agent} # TODO make cnfgrble
        ) do |conn|
          conn.use Faraday::Adapter::Typhoeus
        end
      end
      
      app.helpers OEmbed::Helpers
    end
  
  end
  
  register OEmbed
end