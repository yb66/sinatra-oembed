# -*- encoding: utf-8 -*-
require File.expand_path('../lib/sinatra/oembed/version', __FILE__)

Gem::Specification.new do |gem|
  gem.authors       = ["Iain Barnett"]
  gem.email         = ["iainspeed@gmail.com"]
  gem.description   = %q{Simple oembed collector for Sinatra as an Extension}
  gem.summary       = %q{There's not much more to say.}
  gem.homepage      = ""

  gem.files         = `git ls-files`.split($\)
  gem.executables   = gem.files.grep(%r{^bin/}).map{ |f| File.basename(f) }
  gem.test_files    = gem.files.grep(%r{^(test|spec|features)/})
  gem.name          = "sinatra-oembed"
  gem.require_paths = ["lib"]
  gem.version       = Sinatra::OEmbed::VERSION
  gem.add_dependency("sinatra")
  gem.add_dependency("faraday")
  gem.add_dependency("faraday_middleware")
  gem.add_dependency("typhoeus")
end
