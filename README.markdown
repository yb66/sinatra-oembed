## Sinatra OEmbed ##

This is at alpha. Lots to do!

### TODO ###

* Allow changing of connection types, right now it uses Typhoeus.
* Code coverage.
* More and better specs.
* Hooks to change helpers, errors etc if wanted.
* Set up instructions.
* Contribution instructions.
* A polish and a wax.
* Telekinesis abilities so the requester only has to think about what they want.
* Ability to embed people's darkest secrets on a web page (see Telekinesis above).
* Move Sinatra from specs to example, and make the specs run against the examples.
* Pluggable JSON library.
* More docs.
* A Rakefile would be nice.

### Installation ###

Currently, as I've not released this to rubygems and won't until there's proper specs, via bundler:

    gem "sinatra-single_sign_on", :git => "git@bitbucket.org:yb66/sinatra-oembed.git"
    
or you can download it from here and install it manually, but then you're some kind of l33t haxor demigod, and you probably don't even need these instructions.

### Usage ###

Right now, see the specs, look in the support/shared directory.

### Example OEmbed Data ###

## OEMBED!


    {"provider_url": "http://www.youtube.com/",  
    "thumbnail_url": "http://i4.ytimg.com/vi/_OBlgSz8sSM/hqdefault.jpg",  
    "title": "Charlie bit my finger - again !",  
    "html": "\u003ciframe width=\"459\" height=\"344\" src=\"http://www.youtube.com/embed/_OBlgSz8sSM?fs=1\u0026feature=oembed\" frameborder=\"0\" allowfullscreen\u003e\u003c/iframe\u003e",  
    "author_name": "HDCYT",  
    "height": 344,  
    "thumbnail_width": 480,  
    "width": 459,  
    "version": "1.0",  
    "author_url": "http://www.youtube.com/user/   ",  
    "provider_name": "YouTube",  
    "type": "video",  
    "thumbnail_height": 360}  

### Licence ###

Copyright (c) 2012 Iain Barnett

MIT Licence

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


i.e. be good!