# encoding: UTF-8

require_relative "./spec_helper.rb"


describe "/embed/url" do
  let(:url) { "http://www.youtube.com/watch?v=_OBlgSz8sSM" }
  let(:expected) { 
    %q^{"provider_url": "http:\/\/www.youtube.com\/", "thumbnail_url": "http:\/\/i4.ytimg.com\/vi\/_OBlgSz8sSM\/hqdefault.jpg", "title": "Charlie bit my finger - again !", "html": "\u003ciframe width=\"459\" height=\"344\" src=\"http:\/\/www.youtube.com\/embed\/_OBlgSz8sSM?fs=1\u0026feature=oembed\" frameborder=\"0\" allowfullscreen\u003e\u003c\/iframe\u003e", "author_name": "HDCYT", "height": 344, "thumbnail_width": 480, "width": 459, "version": "1.0", "author_url": "http:\/\/www.youtube.com\/user\/HDCYT", "provider_name": "YouTube", "type": "video", "thumbnail_height": 360}^
  }
  let(:route){ "/embed/url" }
  let(:api_query){ "#{route}?url=#{url}" }
  before {
    warn "api_query: #{api_query}"
    post api_query
  }
  include_context "All pages"
  subject { last_response.body }
  it { should == expected }
end