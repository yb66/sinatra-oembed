# encoding: UTF-8

require 'sinatra/base'
require_relative "../../../lib/sinatra/oembed.rb"
class Example < Sinatra::Base
  register Sinatra::OEmbed

  # @param [String] url URL to get details for.
  post "/embed/url" do
    content_type :json
    url = params["url"]
    warn "url = #{url}"
    halt 400, {error: {url: nil, message: "No url was supplied"}}.to_json if url.nil?
    # TODO check input param validity
    res = get_json_from_endpoint_for url
    warn res
    res
  end  
end

shared_context "All pages" do
  include Rack::Test::Methods
  let(:app){ Example }
  #include Example::RSpec::Helpers
end

shared_examples_for "Any route" do
  subject { last_response }
  it { should be_ok }
end